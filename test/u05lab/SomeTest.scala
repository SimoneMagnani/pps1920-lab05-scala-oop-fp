package u05lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u05lab.code.List._

class SomeTest {

  val l = 10 :: 20 :: 30 :: 40 :: Nil() // same as above

  @Test
  def testIncremental() {
    assertEquals(Some(10), l.head) // 10
    assertEquals(Some(List(20,30,40)), l.tail) // 20,30,40
    assertEquals(List(10,20,30,40,10,20,30,40), l append l) // 10,20,30,40,10,20,30,40
    assertEquals(Some(30), l get 2) // 30
    assertEquals(List("a","a","a","a","a","a","a","a","a","a"), of("a",10)) // a,a,a,..,a
    assertEquals(List("a10","a20"),l filter (_<=20) map ("a"+_) ) // a10, a20

    // Ex. 1: zipRight
    assertEquals(List((10,0), (20,1), (30,2), (40,3)),l.zipRight) // List((10,0), (20,1), (30,2), (40,3))

    // Ex. 2: partition
    assertEquals((List(20,30,40),List(10)),l.partition(_>15)) // (Cons(20,Cons(30,Cons(40,Nil()))), Cons(10,Nil()))

    // Ex. 3: span
    assertEquals((Nil(),List(10,20,30,40)),l.span(_>15)) // ( Nil(), Cons(10,Cons(20,Cons(30,Cons(40,Nil())))) )
    assertEquals((List(10),List(20,30,40)),l.span(_<15)) // ( Cons(10,Nil()), Cons(20,Cons(30,Cons(40,Nil()))) )

    // Ex. 4: reduce
    assertEquals(100,l.reduce(_+_)) // 100
    try { List[Int]().reduce(_+_); assert(false) } catch { case _:UnsupportedOperationException => }

    // Ex. 5: takeRight
    assertEquals(List(30,40),l.takeRight(2)) // Cons(30,Cons(40,Nil()))

    // Ex. 6: collect
    assertEquals(List(9,39),l.collect { case x if x<15 || x>35 => x-1 }) // Cons(9, Cons(39, Nil()))*/
  }
}